# Ansible Pi Cluster

This project manages the configuration of a cluster of Raspberry Pi nodes, used to create various configuration for testing the Home Topology Mapping function in ECO Maange/Assist.

## Creating a Cluster

### Define the new cluster in the _hosts_ file

Example:

        [cluster-good-place]
        pi-eleanor
        pi-chidi
        pi-tahani
        pi-jason
        
This example declares a four-node cluster. The individual nodes will be defined two steps for now.

### Add a play for the new cluster in the _site.yml_ file

Example:

        - name: Apply configuration to nodes in the "Good Place" cluster
          hosts: cluster-good-place
          roles:
            - common
          tags:
            - never
            - cluster-good-place

This play will apply the configuration to the cluster. The "never" tag (as of Ansible version 2.5) prevents
the play from running unless the tag is specifically requested. the "cluster-good-place" is the tag
that needs to be specified to apply the configuration to each cluster.

### Add WiFi configuration

The _roles/common/templates/wpa_supplicant.conf.j2_ file defines all the WiFi access points that
will be used. You can define WiFi connections either by SSID (which will allow the client to connect
to whichever access point it prefers) or by BSSID -- the MAC address for the WiFi interface on a given
access point. Here is an example:

        {% if 'cluster-good-place' in group_names %}
        network={
        
        {{ select_network("crosenthal_24") }}ssid="Frontier8688"
        
        {{ select_network("crosenthal_rgw_24") }}bssid=2C:7E:81:89:1F:10
        
        {{ select_network("crosenthal_ext_24") }}bssid=C0:A0:0D:F3:FD:A9
        
            psk="0768544468"
        }
        {% endif %}

This defines the WiFi network for the "cluster-good-place" cluster. The _crosenthal_24_ entry
defines a connection to the SSID of the WiFi network ("Frontier8688"). The _crosenthal_rgw_24_ and _crosenthal_ext_24_
entries define connections to a specific access point -- either the residential gateway or its
WiFi extender.

The _psk_ entry defines the WiFi password.

The next section describes how to get each node of the cluster to use the configurations defined here.

### Create a _hosts_vars_ file for each node

You must create a _host_vars_ file for each node in the cluster.
The file name must mention the name that you specified in the _hosts_ file.
For example, here is the configuration for _host_vars/pi-eleanor.yml_:

        ---
        hostname: pi-eleanor
        ansible_host: 10.37.82.7
        
        # Force WiFi connection to wpa_supplicant.conf network with this id_str.
        wpa_selected_network: crosenthal_ext_24

The _hostname_ and _ansible_host_ settings must be defined.

The _wpa_selected_network_ definition specifies how to connect to the WiFi network.
The value must match one of the labels created in the _wpa_supplicant.conf.j2_ file described above.

### Push the configuration to the cluster

Push the configuration with the command:

        ansible-playbook -i hosts -l CLUSTER_NAME site.yml
        
The "-l" option is important to ensure that only the configuration for the specified cluster is pushed, and
any other clusters are left undisturbed.

Replace "CLUSTER_NAME" with the name assigned in the _hosts_ file for the selected cluster.
 
If you make changes to the WiFi configuration, you can push just those changes with the command:

        ansible-playbook -i hosts -l CLUSTER --tags=wifi site.yml


## Ansible cheat sheet

Cleanly shutdown all the nodes in the "cluster-good-place" cluster:

    ansible cluster-good-place -i hosts -a "shutdown -h now"
    
Install package updates on the "cluster-good-place" nodes:

    ansible cluster-good-place -i hosts -m apt -a "upgrade=yes"

    
## Creating the SD card image

Copy the Raspberry Pi Lite image to a new SD card. I mastered the initial image with _2018-06-27-raspbian-stretch-lite_.

Boot and perform setup:

* Login as pi/raspberry
* Run: sudo -s
* Run: raspi-config
  * Boot Options -> Desktop CLI -> Console Autologin
  * Localization Options -> Change Locale -> 
    * Remove: en_GB.UTF-8
    * Add: en_US.UTF-8
    * Select: OK
    * Select default locale: en_US.UTF-8
  * Localization Options -> Change Keyboard Layout
    * Select: Generic 104-Key PC
    * Keyboard Layout -> Other
    * Country of origin for the keyboard -> English (US)
    * Keyboard layout -> English (US)
    * Key to function as AltGr -> The default for the keyboard layout
    * Compose key -> No compose key
  * Localization Options -> Change Wi-fi Country -> US
  * Interfacing Options -> SSH -> Yes (enabled)
  * Select: Finish
* When prompted: Would you like to reboot now? -> No

Next, update the system. Run:

    apt update
    apt upgrade

Finally, while the updates run on the console, login via _ssh_ to install _ssh/pi-cluster.pub into _/root/.ssh/authorized_keys_ on the node.
Ensure the device is cabled to wired Ethernet. Then perform:

Run:

    ssh pi@<NODE IP ADDRESS>
    (password: raspberry)
    sudo -s
    cd ~root
    mkdir .ssh
    chmod 700 .ssh
    cd .ssh
    vi authorized_keys
    (deposit pi-cluster.pub here)
    chmod 600 authorized_keys

The default Raspbian Lite image has two partitions, a small MS-DOS partition with drivers and a second Linux partition
that fills to the size of the device. To make the image easier to copy I used _gparted_ to shrink the Linux partition
to 3GB.

Next, I extracted the image from the SD card by running:

        dd if=/dev/sdb of=pi-cluster-20180807.img bs=1M count=4096 status=progress
        
I used the _etcher_ utility (https://etcher.io/) to master new SD cards from this image.